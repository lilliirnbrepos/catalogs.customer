<?xml version="1.0"?>
<model xmlns="http://www.neeveresearch.com/schema/x-ddl" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <env>
    <nv>
      <!-- Discovery for XVMs and cluster members -->
      <discovery.descriptor>mcast://224.0.1.200:4090</discovery.descriptor>
      
      <!-- optimize for throughput -->
      <optimizefor>latency</optimizefor>

      <!-- trace -->
      <aep.trace>info</aep.trace>

      <!-- global latency stats settings -->
      <time.useepochoffset>false</time.useepochoffset>
      <msg.latency.stats>true</msg.latency.stats>
      <ods.latency.stats>true</ods.latency.stats>
      <link.network.stampiots>true</link.network.stampiots>
      <latencymanager.samplesize>10240</latencymanager.samplesize>
    </nv>

    <!-- Configure the test drivers -->
    <driver.autoStart>false</driver.autoStart>
    <driver.sendRate>1000</driver.sendRate>
    <driver.sendCount>1000</driver.sendCount>
  </env>

  <buses>
    <templates>
      <bus name="bus-template" descriptor="${Customerprocessor.bus.descriptor::activemq://localhost:61616}" />
    </templates>

    <bus name="Customerprocessor" template="bus-template">
      <channels>
        <channel name="requests" id="1">
          <qos>Guaranteed</qos>
        </channel>
        <channel name="events" id="2">
          <qos>Guaranteed</qos>
        </channel>
      </channels>
    </bus>
  </buses>

  <apps>
    <templates>
      <app name="app-template">
        <captureTransactionLatencyStats>true</captureTransactionLatencyStats>
        <captureMessageTypeStats>true</captureMessageTypeStats>
        <storage enabled="false">
          <persistenceQuorum>1</persistenceQuorum>
          <clustering enabled="false">
            <localIfAddr>${STORE_REPLICATION_INTERFACE::0.0.0.0}</localIfAddr>
          </clustering>
          <persistence enabled="true">
            <flushUsingMappedMemory>false</flushUsingMappedMemory>
            <flushOnCommit>true</flushOnCommit>
          </persistence>
        </storage>
      </app>
    </templates>
    <app name="Customerprocessor" mainClass="com.lillie.catalogs.customer.Application" template="app-template">
      <messaging>
        <factories>
          <factory name="com.lillie.catalogs.customer.messages.MessageFactory" />
        </factories>
        <bus name="Customerprocessor">
          <channels>
            <channel name="requests" join="true" />
            <channel name="events" join="false" />
          </channels>
        </bus>
      </messaging>
      <storage enabled="true">
        <factories>
          <factory name="com.lillie.catalogs.customer.state.StateFactory" />
        </factories>
        <clustering enabled="true">
        </clustering>
        <persistence enabled="true">
          <initialLogLength>0</initialLogLength>
          <flushOnCommit>true</flushOnCommit>
          <!-- detached persist is not supported for state replication -->
          <detachedPersist enabled="false"/>
        </persistence>
      </storage>
    </app>

    <app name="Customerprocessor-send-driver" mainClass="com.lillie.catalogs.customer.driver.SendDriver" template="app-template">
      <sequenceUnsolicitedSends>true</sequenceUnsolicitedSends>
      <messaging>
        <factories>
          <factory name="com.lillie.catalogs.customer.messages.MessageFactory" />
        </factories>
        <bus name="Customerprocessor">
          <channels>
            <channel name="requests" join="true" />
            <channel name="events" join="true" />
          </channels>
        </bus>
      </messaging>
      <storage enabled="false">
        <clustering enabled="false" />
        <persistence enabled="false">
          <!-- detached persist is not supported for state replication -->
          <detachedPersist enabled="false" />
        </persistence>
      </storage>
    </app>

    <app name="Customerprocessor-receive-driver" mainClass="com.lillie.catalogs.customer.driver.ReceiveDriver" template="app-template">
      <messaging>
        <factories>
          <factory name="com.lillie.catalogs.customer.messages.MessageFactory" />
        </factories>
        <bus name="Customerprocessor">
          <channels>
            <channel name="requests" join="false" />
            <channel name="events" join="true" />
          </channels>
        </bus>
      </messaging>
      <storage enabled="true">
        <clustering enabled="false" />
        <persistence enabled="true">
          <!-- detached persist is not supported for state replication -->
          <detachedPersist enabled="false" />
        </persistence>
      </storage>
    </app>
  </apps>

  <xvms>
    <templates>
      <xvm name="xvm-template">
        <heartbeats enabled="true" interval="5">
          <tracing enabled="false">
            <traceAdminClientStats>false</traceAdminClientStats>
            <traceAppStats>false</traceAppStats>
            <tracePoolStats>false</tracePoolStats>
            <traceSysStats>false</traceSysStats>
            <traceThreadStats>false</traceThreadStats>
            <traceUserStats>true</traceUserStats>
          </tracing>
          <collectIndividualThreadStats>true</collectIndividualThreadStats>
          <collectSeriesStats>true</collectSeriesStats>
          <includeMessageTypeStats>true</includeMessageTypeStats>
        </heartbeats>
      </xvm>
    </templates>
    <xvm name="Customerprocessor-1" template="xvm-template">
      <apps>
        <app name="Customerprocessor" />
      </apps>
    </xvm>
    <xvm name="Customerprocessor-2" template="xvm-template">
      <apps>
        <app name="Customerprocessor" autoStart="true" />
      </apps>
    </xvm>
    <xvm name="Customerprocessor-send-driver" template="xvm-template">
      <apps>
        <app name="Customerprocessor-send-driver" autoStart="true" />
      </apps>
    </xvm>
    <xvm name="Customerprocessor-receive-driver" template="xvm-template">
      <apps>
        <app name="Customerprocessor-receive-driver" autoStart="true" />
      </apps>
    </xvm>
  </xvms>

  <profiles>
    <profile name="test">
      <env>
        <nv>
          <conservecpu>true</conservecpu>
        </nv>
        <driver.autoStart>true</driver.autoStart>
      </env>
    </profile>

    <profile name="desktop">
      <env>
        <nv>
          <data.directory>rdat/${nv.ddl.targetxvm::.}</data.directory>
          <conservecpu>true</conservecpu>
        </nv>
        <driver.autoStart>true</driver.autoStart>
      </env>

      <xvms>
        <templates>
          <xvm name="xvm-template">
            <heartbeats enabled="true" interval="5">
              <tracing enabled="true">
                <traceUserStats>true</traceUserStats>
              </tracing>
            </heartbeats>
          </xvm>
        </templates>
      </xvms>
    </profile>

    <profile name="production">
      <activation>
        <properties>
          <lumino.agent.env>production</lumino.agent.env>
        </properties>
      </activation>
      <env>
        <nv>
          <discovery.descriptor>mcast://224.0.1.200:4090</discovery.descriptor>
        </nv>
        <STORE_LINK_PARAMS>nativeio=true,eagerread=true,maxreadspintime=1000000</STORE_LINK_PARAMS>
      </env>

      <xvms>
        <templates>
          <xvm name="xvm-template">
            <env>
              <GC_HEAP_SIZE_PARAMS>-Xms4g -Xmx4g</GC_HEAP_SIZE_PARAMS>
              <GC_TUNING_PARAMS>-XX:MaxNewSize=4m -XX:SurvivorRatio=6 -XX:+UseParNewGC -XX:ParallelGCThreads=4 -Xnoclassgc -XX:MaxTenuringThreshold=2</GC_TUNING_PARAMS>
            </env>
            <provisioning>
              <jvm>
                <!--javaHome>/usr/java/jdk1.8.0_60</javaHome>-->
                <jvmParamSets>
                  <jvmParamSet name="heap-size" order="-1">
                    <jvmParams>${GC_HEAP_SIZE_PARAMS}</jvmParams>
                  </jvmParamSet>
                  <jvmParamSet name="gc-tuning" order="0">
                    <jvmParams>${GC_TUNING_PARAMS}</jvmParams>
                  </jvmParamSet>
                </jvmParamSets>
              </jvm>
            </provisioning>
          </xvm>
        </templates>
        <xvm name="Customerprocessor-1">
          <env>
            <GC_HEAP_SIZE_PARAMS>-Xms4g -Xmx4g</GC_HEAP_SIZE_PARAMS>
            <STORE_REPLICATION_INTERFACE>0.0.0.0</STORE_REPLICATION_INTERFACE>
          </env>
          <provisioning>
            <host>.</host>
          </provisioning>
        </xvm>
        <xvm name="Customerprocessor-2">
          <env>
            <GC_HEAP_SIZE_PARAMS>-Xms4g -Xmx4g</GC_HEAP_SIZE_PARAMS>
            <STORE_REPLICATION_INTERFACE>0.0.0.0</STORE_REPLICATION_INTERFACE>
          </env>
          <provisioning>
            <host>.</host>
          </provisioning>
        </xvm>
        <xvm name="Customerprocessor-send-driver" enabled="true">
          <provisioning>
            <host>.</host>
          </provisioning>
        </xvm>
        <xvm name="Customerprocessor-receive-driver" enabled="true">
          <provisioning>
            <host>.</host>
          </provisioning>
        </xvm>
      </xvms>
    </profile>

    <profile name="activemq">
      <env>
        <ACTIVEMQ_ADDRESS>localhost:61616</ACTIVEMQ_ADDRESS>
        <nv>
          <discovery.descriptor>activemq://localhost:61616&amp;initWaitTime=0</discovery.descriptor>
        </nv>
      </env>
      <buses>
        <templates>
          <bus name="bus-template" descriptor="${Customerprocessor.bus.descriptor::activemq://${ACTIVEMQ_ADDRESS}}"/>
        </templates>
      </buses>
    </profile>

    <profile name="loopback">
      <env>
        <LOOPBACK_BUSNAME>test</LOOPBACK_BUSNAME>
        <nv>
          <discovery.descriptor>loopback://test&amp;initWaitTime=0</discovery.descriptor>
        </nv>
      </env>
      <buses>
        <templates>
          <bus name="bus-template" descriptor="${Customerprocessor.bus.descriptor::loopback://${dollarySymbol}{LOOPBACK_BUSNAME::.}}" />
        </templates>
      </buses>
    </profile>
  </profiles>
</model>