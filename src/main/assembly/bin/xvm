#!/bin/bash
# ===
# Copyright (c) 2018 Neeve Research, LLC. All Rights Reserved.
# Confidential and proprietary information of Neeve Research, LLC.
# Copyright Version 1.0
# ===

# Switch to installation root
pushd `dirname $0`/.. > /dev/null
status=0

# Validate that JAVA_HOME is set
if [ "$JAVA_HOME" = "" ]
then
   echo The JAVA_HOME environment variable needs to be set.
   exit 1
fi

# Display help if no args
if [ "$#" == "0" ] 
then 
  $JAVA_HOME/bin/java -cp "libs/*" com.neeve.server.Main --help
  exit 1
fi

# Pre-parse args to find the XVM name
XVM=default
ARGS=$*
while [ "$1" != "" ]
do
  if [ "$1" = "-n" -o "$1" = "--name" ]
  then
    shift
    XVM=$1
  fi
  shift
done  

# Set up heap size and GC
GC_OPTS="-Xms1572m -Xmx1572m"

# Ensure trace log dirs exist
mkdir -p rdat/logs

# Set the native library path
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:~/.nvx/native

# Run
$JAVA_HOME/bin/java $GC_OPTS -Djava.library.path="$LD_LIBRARY_PATH" -cp "libs/*" com.neeve.server.Main $ARGS 2>&1 | tee rdat/logs/$XVM.log 
status=$?

popd
exit $status