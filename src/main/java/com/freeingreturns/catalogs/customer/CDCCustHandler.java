/****************************************************************************
 * FILE: CDCHandler.java
 * DSCRPT:
 ****************************************************************************/

package com.freeingreturns.catalogs.customer;

import java.util.List;

import com.eaio.uuid.UUID;
import com.freeingreturns.catalogs.customer.state.Customer;
import com.freeingreturns.utils.FRLogger;
import com.neeve.rog.IRogChangeDataCaptureHandler;
import com.neeve.rog.IRogNode;
import com.neeve.trace.Tracer.Level;

import freeingreturns.catalogs.db.LocalIMDB;
import freeingreturns.catalogs.orm.DbCustomer;

public class CDCCustHandler implements IRogChangeDataCaptureHandler {

    private LocalIMDB localDb;

    boolean inserted = false;
    Customer cst;
    DbCustomer dbCst;





    /**
     *
     */
    public CDCCustHandler() {
        FRLogger.logAsIs(Level.SEVERE, "CDCCustHandler::CDCCustHandler()");
        localDb = LocalIMDB.getInstance();
        localDb.connectToDb();
    }





    @Override
    public boolean handleChange(UUID objectId, ChangeType changeType, List<IRogNode> entries) {

        if (changeType == ChangeType.Send || changeType == ChangeType.Noop) {
            return true;
        }

        FRLogger.logfmt(Level.INFO, "here");

        for (IRogNode node : entries) {

            if (!(node instanceof Customer))
                continue;

            inserted = false;
            cst = (Customer)node;
            dbCst = new DbCustomer();
            dbCst.getCustomerID().setValue(cst.getCustomerID());
            dbCst.getPartyID().setValue(cst.getPartyID());
            dbCst.getCustomerStatus().setValue(cst.getCustomerStatus());
            dbCst.getPricingGroupID().setValue(cst.getPricingGroupID());
            dbCst.getCustomerBatchID().setValue(cst.getCustomerBatchID());

            if (changeType.equals(ChangeType.Put))
                inserted = localDb.addCustomer(dbCst);
            else if (changeType.equals(ChangeType.Update))
                inserted = localDb.updateCustomer(dbCst);

            FRLogger.logfmt(Level.INFO, "[dbworker] - success:{0}, table:{1}, id:{2}",
                            Boolean.toString(inserted),
                            dbCst.getTableName(),
                            dbCst.getCustomerID().getValue());
        }
        return true;
    }





    @Override
    public boolean onCheckpointComplete(long checkpointVersion) {
        return true;
    }





    @Override
    public void onCheckpointStart(long checkpointVersion) {}





    @Override
    public void onLogComplete(int logNumber, LogCompletionReason reason, Throwable errorCause) {}





    @Override
    public void onLogStart(int logNumber) {}





    @Override
    public void onWait() {}

}
