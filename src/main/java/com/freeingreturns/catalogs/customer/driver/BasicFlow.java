package com.freeingreturns.catalogs.customer.driver;

import static java.text.MessageFormat.format;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

import com.freeingreturns.roe.customer.RplyCustomerAdd;
import com.freeingreturns.roe.customer.RplyCustomerQuery;
import com.freeingreturns.roe.customer.RqstClearAllCustomers;
import com.freeingreturns.roe.customer.RqstCustomerAdd;
import com.freeingreturns.roe.customer.RqstCustomerQuery;
import com.neeve.aep.AepEngine;
import com.neeve.aep.AepMessageSender;
import com.neeve.aep.annotations.EventHandler;
import com.neeve.cli.annotations.Argument;
import com.neeve.cli.annotations.Command;
import com.neeve.cli.annotations.Configured;
import com.neeve.server.app.annotations.AppInjectionPoint;
import com.neeve.server.app.annotations.AppMain;
import com.neeve.server.app.annotations.AppStat;
import com.neeve.stats.IStats.Counter;
import com.neeve.stats.StatsFactory;
import com.neeve.trace.Tracer;
import com.neeve.trace.Tracer.Level;
import com.neeve.util.UtlGovernor;

import freeingreturns.catalogs.datagen.CustomerGenerator;
import freeingreturns.catalogs.datagen.IGenerator;
import freeingreturns.catalogs.orm.DbCustomer;

/**
 * A test driver app for the Application.
 */
public class BasicFlow {
    private Tracer logger = Tracer.create("service.customer", Level.FINEST);

    private final AtomicReference<Thread> sendingThread = new AtomicReference<Thread>();
    private volatile AepMessageSender messageSender;
    private volatile AepEngine engine;

    private Hashtable<Long, DbCustomer> customerList;
    private List<DbCustomer> customers;
    private CountDownLatch latch;

    @Configured(property = "service.customer.tests.basic.autoStart")
    private boolean autoStart;

    @Configured(property = "service.customer.tests.basic.sendCount")
    private int sendCount;

    @Configured(property = "service.customer.tests.basic.sendRate")
    private int sendRate;

    @Configured(property = "service.customer.channels.requestChannel", defaultValue = "CustomerReplyChannel")
    private String sendChannel = "CustomerRequestChannel";

    @Configured(property = "service.customer.tests.basic.clearCustomers", defaultValue = "false")
    private boolean clearCustomers;
    @AppStat
    private final Counter sentCount = StatsFactory.createCounterStat("MsgsSent Count");

    public BasicFlow() {
        customerList = new Hashtable<>();

    }

    @AppInjectionPoint
    final public void setMessageSender(AepMessageSender messageSender) {
        this.messageSender = messageSender;
    }

    @AppInjectionPoint
    final public void setAepEngine(AepEngine engine) {
        this.engine = engine;
    }

    /**
     * Starts sending messages (in a background thread)
     */
    @Command(name = "send", displayName = "Send Messages", description = "Instructs the driver to send messages")
    public final void doSend(@Argument(name = "count", position = 1, required = true, description = "The number of messages to send") final int count,
                             @Argument(name = "rate", position = 2, required = true, description = "The rate at which to send") final int rate) throws Exception {

        // loopback only
        engine.waitForMessagingToStart();

        if (clearCustomers) {
            RqstClearAllCustomers clear = RqstClearAllCustomers.create();
            clear.setMsgId(System.currentTimeMillis());
            messageSender.sendMessage(sendChannel, clear);
        }

        // generate customers
        final IGenerator<DbCustomer> cGenerator = new CustomerGenerator();
        customers = cGenerator.generate(count);

        // send messages
        final Thread thread = new Thread(new SendWorker(rate, count), "Customerprocessor Send Driver");

        Thread oldThread = sendingThread.getAndSet(thread);
        if (oldThread != null) {
            oldThread.interrupt();
            oldThread.join();
        }
        thread.start();
    }

    /**
     * Stops the current sending thread (if active_ 
     */
    @Command(name = "stopSending", displayName = "Stop Sending", description = "Stops sending of messages.")
    final public void stop() throws Exception {
        Thread oldThread = sendingThread.getAndSet(null);
        if (oldThread != null) {
            oldThread.join();
        }
    }

    /**
     * Gets the number of messages sent by the sender. 
     * 
     * @return The number of messages sent by this sender.
     */
    @Command
    public long getSentCount() {
        return sentCount.getCount();
    }

    @AppMain
    public void run(String[] args) throws Exception {
        if (autoStart) {
            doSend(sendCount, sendRate);
        }
    }

    @EventHandler
    public final void onEvent(RplyCustomerAdd message) {
        if (customerList.get(message.getMsgId()) == null) {
            logger.log(format("Unable to match add-message id:{0}", message.getMsgId()), Level.SEVERE);
            return;
        }

        DbCustomer customer = customerList.remove(message.getMsgId());
        logger.log(format("Matched add-message:{0}, value:{1}", message.getMsgId(), customer.getCustomerID().getValue()), Level.INFO);
	if ( latch!=null )
	  latch.countDown();
        return;
    }

    @EventHandler
    public final void onEvent(RplyCustomerQuery message) {
        latch.countDown();
        customerList.remove(message.getMsgId());
        logger.log(format("Query message:{0}, value:{1}", message.getMsgId(), message.getFound()), Level.INFO);
    }

    public boolean hasUnmatchedMessages() {
        return customerList.keySet().size() > 0;
    }

    public final CountDownLatch getLatch() {
        return latch;
    }

    public void sendQueryMessages() {
        latch = new CountDownLatch(customers.size());
        Iterator<DbCustomer> itr = customers.iterator();
        RqstCustomerQuery qry;
        int cntr = 0;
        while (itr.hasNext()) {
            qry = RqstCustomerQuery.create();
            qry.setCustomerID(itr.next().getCustomerID().getValue());
            qry.setMsgId(cntr++);
            messageSender.sendMessage(sendChannel, qry);
            logger.log(qry.toString(), Level.INFO);
        }
    }

    private class SendWorker implements Runnable {
        private int _rate;

        public SendWorker(int rate, int count) {
            _rate = rate;
        }

        /** 
         * (non-Javadoc)
         * @see java.lang.Runnable#run()
         */
        @Override
        public void run() {
            final UtlGovernor sendGoverner = new UtlGovernor(_rate);
            int sent = 0;

            DbCustomer customer;
            RqstCustomerAdd message;
            Iterator<DbCustomer> cItr = customers.iterator();
            while (cItr.hasNext()) {
                if (sendingThread.get() != Thread.currentThread())
                    break; // someone swapped me out

                customer = cItr.next();
                message = RqstCustomerAdd.create();
                message.setMsgId(sentCount.getCount());
                message.setCustomerID(customer.getCustomerID().getValue());
                message.setPartyID(customer.getPartyID().getValue());
                message.setCustomerStatus(customer.getCustomerStatus().getValue());
                message.setPricingGroupID(customer.getPricingGroupID().getValue());
                message.setCustomerBatchID(customer.getCustomerBatchID().getValue());

                customerList.put(message.getMsgId(), customer);
                messageSender.sendMessage(sendChannel, message);
                sentCount.increment();
                sendGoverner.blockToNext();

                if (logger.isEnabled(Level.DIAGNOSE))
                    logger.log(format("sent:{0}", message.getMsgId()), Level.DIAGNOSE);
            }
        }

    }
}
