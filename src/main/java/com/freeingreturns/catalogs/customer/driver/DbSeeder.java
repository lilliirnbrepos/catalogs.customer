package com.freeingreturns.catalogs.customer.driver;

import static java.text.MessageFormat.format;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

import com.freeingreturns.roe.customer.RplyCustomerAdd;
import com.freeingreturns.roe.customer.RqstCustomerAdd;
import com.neeve.aep.AepEngine;
import com.neeve.aep.AepMessageSender;
import com.neeve.aep.annotations.EventHandler;
import com.neeve.cli.annotations.Command;
import com.neeve.cli.annotations.Configured;
import com.neeve.server.app.annotations.AppInjectionPoint;
import com.neeve.server.app.annotations.AppMain;
import com.neeve.server.app.annotations.AppStat;
import com.neeve.stats.IStats.Counter;
import com.neeve.stats.StatsFactory;
import com.neeve.trace.Tracer;
import com.neeve.trace.Tracer.Level;
import com.neeve.util.UtlGovernor;

import freeingreturns.catalogs.db.LocalIMDB;
import freeingreturns.catalogs.orm.DbCustomer;

/**
 * A test driver app for the Application.
 */
public class DbSeeder implements Runnable {
    private static final Tracer logger = Tracer.create("service.customer", Level.VERBOSE);

    private final AtomicReference<Thread> sendingThread = new AtomicReference<Thread>();
    private volatile AepMessageSender messageSender;
    private volatile AepEngine engine;

    private Hashtable<Long, DbCustomer> customerList;
    private List<DbCustomer> customers;
    private LocalIMDB localDb;
    private CountDownLatch latch;

    @Configured(property = "service.customer.seeder.autoStart")
    private boolean autoStart;

    @Configured(property = "service.customer.channels.replyChannel")
    private String rplyChannel;

    @Configured(property = "service.customer.channels.requestChannel", defaultValue = "CustomerReplyChannel")
    private String sendChannel = "CustomerRequestChannel";

    @Configured(property = "service.customer.seeder.msgsPerSecond")
    private int sendRate;

    @AppStat
    private final Counter sentCount = StatsFactory.createCounterStat("SendDriver Count");

    public DbSeeder() {
        customerList = new Hashtable<>();
        localDb = LocalIMDB.getInstance();
        localDb.connectToDb();
        localDb.loadCustomers();
        customers = localDb.getCustomers();
        latch = new CountDownLatch(customers.size());
    }

    @AppInjectionPoint
    final public void setMessageSender(AepMessageSender messageSender) {
        this.messageSender = messageSender;
    }

    @AppInjectionPoint
    final public void setAepEngine(AepEngine engine) {
        this.engine = engine;
    }

    /**
     * Starts sending messages (in a background thread)
     */
    @Command(name = "sendCustomers", displayName = "Seed customers from DB", description = "Instructs the driver to read customers from DB and seed the catalog service")
    public final void doSeed() throws Exception {

        // loopback only
        engine.waitForMessagingToStart();

        // some info
        logger.log(format("Total number of customers for db seeding:{0}", customers.size()), Level.INFO);

        // send messages
        final Thread thread = new Thread(this, "Customerprocessor Send Driver");
        startSending(thread);
    }

    /**
     * 
     */
    private void startSending(final Thread thread) throws InterruptedException {
        Thread oldThread = sendingThread.getAndSet(thread);
        if (oldThread != null) {
            oldThread.interrupt();
            oldThread.join();
        }
        thread.start();
    }

    /**
     * Stops the current sending thread (if active_ 
     */
    @Command(name = "stopSending", displayName = "Stop Sending", description = "Stops sending of messages.")
    final public void stop() throws Exception {
        Thread oldThread = sendingThread.getAndSet(null);
        if (oldThread != null) {
            oldThread.join();
        }
    }

    /**
     * Gets the number of messages sent by the sender. 
     * 
     * @return The number of messages sent by this sender.
     */
    @Command
    public long getSentCount() {
        return sentCount.getCount();
    }

    @AppMain
    public void run(String[] args) throws Exception {
        if (autoStart) {
            doSeed();
        }
    }

    @EventHandler
    public final void onEvent(RplyCustomerAdd message) {
        if (customerList.get(message.getMsgId()) == null) {
            logger.log(format("Unable to match message id:{0}", message.getMsgId()), Level.SEVERE);
            return;
        }

        DbCustomer customer = customerList.remove(message.getMsgId());
        latch.countDown();
        if (logger.isEnabled(Level.FINEST))
            logger.log(format("Matched message:{0}, value:{1}", message.getMsgId(), customer.getCustomerID().getValue()), Level.FINEST);
    }

    public CountDownLatch getLatch() {
        return latch;
    }

    /** 
     * (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {
        final UtlGovernor sendGoverner = new UtlGovernor(sendRate);

        DbCustomer customer;
        RqstCustomerAdd message;
        Iterator<DbCustomer> cItr = customers.iterator();
        while (cItr.hasNext()) {

            if (sendingThread.get() != Thread.currentThread())
                break; // someone swapped me out

            customer = cItr.next();
            message = RqstCustomerAdd.create();
            message.setMsgId(sentCount.getCount());
            message.setCustomerID(customer.getCustomerID().getValue());
            message.setPartyID(customer.getPartyID().getValue());
            message.setCustomerStatus(customer.getCustomerStatus().getValue());
            message.setPricingGroupID(customer.getPricingGroupID().getValue());
            message.setCustomerBatchID(customer.getCustomerBatchID().getValue());

            customerList.put(message.getMsgId(), customer);
            messageSender.sendMessage(sendChannel, message);
            sentCount.increment();
            sendGoverner.blockToNext();

            if (logger.isEnabled(Level.DIAGNOSE))
                logger.log(format("sent:{0}", message.getMsgId()), Level.DIAGNOSE);

        }
    }

}
