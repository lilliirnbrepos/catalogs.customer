package com.freeingreturns.catalogs.customer;

import com.freeingreturns.catalogs.customer.state.Customer;
import com.freeingreturns.catalogs.customer.state.Repository;
import com.freeingreturns.roe.customer.RplyClearAllCustomers;
import com.freeingreturns.roe.customer.RplyCustomerAdd;
import com.freeingreturns.roe.customer.RplyCustomerQuery;
import com.freeingreturns.roe.customer.RplyCustomerQueryTransactions;
import com.freeingreturns.roe.customer.RplyCustomerStsChng;
import com.freeingreturns.roe.customer.RqstClearAllCustomers;
import com.freeingreturns.roe.customer.RqstCustomerAdd;
import com.freeingreturns.roe.customer.RqstCustomerQuery;
import com.freeingreturns.roe.customer.RqstCustomerQueryTransactions;
import com.freeingreturns.roe.customer.RqstCustomerStsChng;
import com.freeingreturns.roe.global.IdentityKeysDTO;
import com.freeingreturns.roe.txmanager.RplyTxmCreateGroup;
import com.freeingreturns.roe.txmanager.TxGroupDTO;
import com.freeingreturns.utils.FRLogger;
import com.freeingreturns.utils.FRUnhandleExcpetionCatcher;
import com.neeve.aep.AepEngine;
import com.neeve.aep.AepMessageSender;
import com.neeve.aep.IAepApplicationStateFactory;
import com.neeve.aep.annotations.EventHandler;
import com.neeve.aep.event.AepEngineStoppingEvent;
import com.neeve.aep.event.AepMessagingStartedEvent;
import com.neeve.cli.annotations.Command;
import com.neeve.cli.annotations.Configured;
import com.neeve.rog.log.RogLog;
import com.neeve.rog.log.RogLogCdcProcessor;
import com.neeve.server.app.annotations.AppHAPolicy;
import com.neeve.server.app.annotations.AppInjectionPoint;
import com.neeve.server.app.annotations.AppStateFactoryAccessor;
import com.neeve.sma.MessageView;
import com.neeve.trace.Tracer.Level;

@AppHAPolicy(value = AepEngine.HAPolicy.StateReplication)
public class CustomerCatalogMain {

    private CDCCustHandler handler;
    private RogLog log;
    private RogLogCdcProcessor processor;

    private AepEngine aepEngine;
    private AepMessageSender messageSender;

    @Configured(property = "service.customer.returnValueOnQueryFail", defaultValue = "false")
    private boolean returnValueOnQueryFail;

    @Configured(property = "service.customer.channels.replyChannel")
    private String outboundChannel;

    private int msgCnt = 0;





    public CustomerCatalogMain() {
        Thread.setDefaultUncaughtExceptionHandler(FRUnhandleExcpetionCatcher.getInstance());
    }





    @AppStateFactoryAccessor
    final public IAepApplicationStateFactory getStateFactory() {
        return new IAepApplicationStateFactory() {
            @Override
            final public Repository createState(MessageView view) {
                return Repository.create();
            }
        };
    }





    @AppInjectionPoint
    final public void setMessageSender(AepMessageSender msgSender) {
        messageSender = msgSender;
    }





    @AppInjectionPoint
    public void injectAepEngine(AepEngine engine) {
        aepEngine = engine;
    }





    @EventHandler
    final public void onMessagingStarted(final AepMessagingStartedEvent event) {
        Thread t = new Thread(new Runnable() {
            public void run() {
                FRLogger.logAsIs(Level.INFO, "Engine activated, starting CDC");
                try {
                    startCdc();
                }
                catch (Exception e_) {
                    e_.printStackTrace();
                }
            }
        });
        t.start();
    }





    @EventHandler
    public void onEngineStopped(AepEngineStoppingEvent event) {
        FRLogger.logAsIs(Level.INFO, "Engine stopping, stopping CDC");
        try {
            stopCdc();
        }
        catch (Exception e_) {
            e_.printStackTrace();
        }
    }





    @Command(name = "Start CDC")
    public void startCdc() throws Exception {
        Boolean enabled = false;
        try {
            enabled = Boolean.valueOf(aepEngine.getStore().getPersister().getDescriptor().getProperty(RogLog.PROP_CDC_ENABLED));
            if (enabled) {
                log = (RogLog)aepEngine.getStore().getPersister();
                handler = new CDCCustHandler();
                processor = log.createCdcProcessor(handler);
                processor.run();
                return;
            }
        }
        catch (Exception e) {}
        FRLogger.logAsIs(Level.WARNING, "CDC Storage disabled");
    }





    @Command(name = "Stop CDC")
    public void stopCdc() throws Exception {
        if (processor != null)
            processor.close();
    }





    @Command(name = "Clear customers")
    public void clearCustomers() throws Exception {
        RqstClearAllCustomers rqst = RqstClearAllCustomers.create();
        rqst.setMsgId(666);
        aepEngine.injectMessage(rqst);
        FRLogger.logAsIs(Level.WARNING, "Clearing all customers");
    }





    @EventHandler
    final public void onMessage(RqstCustomerAdd msg, Repository repo) {
        RplyCustomerAdd rply = RplyCustomerAdd.create();
        rply.setMsgId(msg.getMsgId());
        rply.setCustomerID(msg.getCustomerID());

        // make sure the customer id is ok
        if (validateCustomerID(msg.getCustomerID())) {
            rply.setSucceeded(false);
            rply.setErrorString("bad customer-id");
            messageSender.sendMessage(outboundChannel, rply);
            FRLogger.logfmt(Level.INFO, msg.getMsgId(), msg.getMessageChannel(),
                            "Customer id is invalid: {0}", msg.getCustomerID());
            return;
        }

        // make sure I don't already know this customer
        Customer customer = repo.getCustomerRepo().get(msg.getCustomerID());
        if (customer != null) {
            rply.setSucceeded(false);
            rply.setErrorString("duplicate entry");
            messageSender.sendMessage(outboundChannel, rply);
            FRLogger.logfmt(Level.SEVERE, msg.getMsgId(), msg.getMessageChannel(),
                            "Customer already present: {0}", msg.getCustomerID());
            return;
        }

        // nope, add it
        customer = Customer.create();
        customer.setCustomerID(msg.getCustomerID());
        customer.setPartyID(msg.getPartyID());
        customer.setCustomerStatus(msg.getCustomerStatus());
        customer.setPricingGroupID(msg.getPricingGroupID());
        customer.setCustomerBatchID(msg.getCustomerBatchID());

        // add, send & log
        repo.getCustomerRepo().put(msg.getCustomerID(), customer);
        rply.setSucceeded(true);
        messageSender.sendMessage(outboundChannel, rply);
        FRLogger.logfmt(Level.DIAGNOSE, msg.getMsgId(), msg.getMessageChannel(),
                        "customer added: {0}", msg.getCustomerID());
    }





    @EventHandler
    final public void onMessage(RqstCustomerStsChng msg, Repository repo) {
        RplyCustomerStsChng rply = RplyCustomerStsChng.create();
        rply.setMsgId(msg.getMsgId());
        rply.setSucceeded(false);
        messageSender.sendMessage(outboundChannel, rply);
        FRLogger.logfmt(Level.DIAGNOSE, msg.getMsgId(), msg.getMessageChannel(),
                        "{0}", "Customer status change: Not implmented yet!");
    }





    @EventHandler
    final public void onMessage(RplyTxmCreateGroup msg, Repository repo) {
        if (!msg.getSucceeded()) {
            FRLogger.logfmt(Level.DIAGNOSE, msg.getMsgId(), msg.getMessageChannel(),
                            "received failed tx-group - not tracking: {0}", msg.getTxGroupDTO().getCompoundTransactionId());
            return;
        }

        Customer cust = repo.getCustomerRepo().get(msg.getTxGroupDTO().getCustomerID());
        if (cust == null) {
            FRLogger.logfmt(Level.SEVERE, msg.getMsgId(), msg.getMessageChannel(),
                            "ERROR: Unable to find customer: {1}, cannot track: {0}",
                            msg.getTxGroupDTO().getCompoundTransactionId(),
                            msg.getTxGroupDTO().getCustomerID());
            return;
        }

        TxGroupDTO dto = msg.takeTxGroupDTO();
        cust.addTransactions(dto);
        FRLogger.logfmt(Level.FINE, msg.getMsgId(), msg.getMessageChannel(),
                        "added tx group: {0} to customer: {1}",
                        msg.getTxGroupDTO().getCompoundTransactionId(),
                        msg.getTxGroupDTO().getCustomerID());
        return;
    }





    @EventHandler
    final public void onMessage(RqstCustomerQuery msg, Repository repo) {
        RplyCustomerQuery rply = RplyCustomerQuery.create();
        rply.setMsgId(msg.getMsgId());
        rply.setCustomerID(msg.getCustomerID());

        // make sure the customer id is ok
        if (validateCustomerID(msg.getCustomerID())) {
            rply.setFound(false);
            messageSender.sendMessage(outboundChannel, rply);
            FRLogger.logfmt(Level.DIAGNOSE, msg.getMsgId(), msg.getMessageChannel(),
                            "unable to lookup coustomer:{0}", msg.getCustomerID());
            return;
        }

        // actually look for it
        Customer customer = repo.getCustomerRepo().get(msg.getCustomerID());
        if (customer == null) {
            rply.setFound(returnValueOnQueryFail);
            messageSender.sendMessage(outboundChannel, rply);
            FRLogger.logfmt(Level.WARNING, msg.getMsgId(), msg.getMessageChannel(),
                            "query failed: {0}", msg.getCustomerID());
            return;
        }

        // found it
        rply.setPartyID(customer.getPartyID());
        rply.setCustomerStatus(customer.getCustomerStatus());
        rply.setPricingGroupID(customer.getPricingGroupID());
        rply.setCustomerBatchID(customer.getCustomerBatchID());
        rply.setFound(true);

        // send & log
        messageSender.sendMessage(outboundChannel, rply);
        FRLogger.logfmt(Level.DIAGNOSE, msg.getMsgId(), msg.getMessageChannel(),
                        "query succeeded: {0}", msg.getCustomerID());

    }





    @EventHandler
    final public void onMessage(RqstClearAllCustomers msg, Repository repo) {
        FRLogger.logfmt(Level.WARNING, msg.getMsgId(), msg.getMessageChannel(),
                        "{0}", "Clearing all customers!");
        //repo.getCustomerRepo().clear();
        RplyClearAllCustomers rply = RplyClearAllCustomers.create();
        rply.setErrorString("not enabled");
        rply.setMsgId(msg.getMsgId());
        rply.setSucceeded(true);
        messageSender.sendMessage(outboundChannel, rply);
    }





    @EventHandler
    final public void onMessage(RqstCustomerQueryTransactions msg, Repository repo) {
        RplyCustomerQueryTransactions rply = RplyCustomerQueryTransactions.create();
        rply.setMsgId(msg.getMsgId());
        rply.setCustomerID(msg.getCustomerID());

        Customer customer = repo.getCustomerRepo().get(msg.getCustomerID());
        if (customer == null) {
            rply.setFound(false);
            messageSender.sendMessage(outboundChannel, rply);
            FRLogger.logfmt(Level.SEVERE, msg.getMsgId(), msg.getMessageChannel(),
                            "CST-TX Query failed: Customer NOT present for ID: {0}", msg.getCustomerID());
            return;
        }

        int sz = 0;
        TxGroupDTO[] txs = customer.getTransactions();
        if (txs != null) {
            IdentityKeysDTO dstDto;
            sz = txs.length;
            for (TxGroupDTO item : txs) {
                dstDto = IdentityKeysDTO.create();
                dstDto.setRetailStoreID(item.getRetailStoreID());
                dstDto.setBusinessDay(item.getBusinessDay());
                dstDto.setTransactionSequenceNumber(item.getTransactionSequenceNumber());
                dstDto.setWorkstationID(item.getWorkstationID());
                rply.addTransactions(dstDto);
            }
        }

        rply.setFound(true);

        // send & log
        messageSender.sendMessage(outboundChannel, rply);
        FRLogger.logfmt(Level.DIAGNOSE, msg.getMsgId(), msg.getMessageChannel(),
                        "query succeeded: {0}. Total tx-count: {1}", msg.getCustomerID(), sz);
        return;
    }





    private boolean validateCustomerID(String customerId) {
        if (customerId == null || "".equals(customerId))
            return true;
        return false;
    }

}
