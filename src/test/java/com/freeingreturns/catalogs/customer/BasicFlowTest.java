package com.freeingreturns.catalogs.customer;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.junit.Test;

import com.freeingreturns.catalogs.customer.CustomerCatalogMain;
import com.freeingreturns.catalogs.customer.driver.BasicFlow;
import com.neeve.trace.Tracer;
import com.neeve.trace.Tracer.Level;

/**
 * A test case that tests the application flow. 
 */
public class BasicFlowTest extends AbstractTest {
    private static final Tracer logger = Tracer.create("service.customer", Level.FINEST);

    private Properties setupEnv(int sendCount, int sendRate) {
        // configure
        Properties env = new Properties();
        env.put("nv.ddl.profiles", "junit");
        env.put("x.apps.cst-processor.storage.clustering.enabled", "false");
        env.put("x.apps.cst-dbseeder.storage.clustering.enabled", "false");
        env.put("x.apps.cst-basicflow.storage.clustering.enabled", "false");
        env.put("appdataroot", "target/testbed/rdat");
        return env;
    }

    @Test
    public void testFlowAdd() throws Throwable {
        int sendCount = 1;
        int sendRate = 100;
        Properties env = setupEnv(sendCount, sendRate);

        // start apps
        startApp(CustomerCatalogMain.class, "cst-processor", "cst-instA", env);

        // start the send driver
        BasicFlow sendDriver = startApp(BasicFlow.class, "cst-basicflow", "cst-utils", env);

        do {
            waitForSomeTime(1000);
            logger.log("waiting...", Level.INFO);
        }
        while (sendDriver.hasUnmatchedMessages() && sendDriver.getSentCount() < sendCount);

        // query a customer
        sendDriver.sendQueryMessages();
        sendDriver.getLatch().await(100000, TimeUnit.MILLISECONDS);

    }

    /**
     * 
     */
    private void waitForSomeTime(int time) {
        try {
            Thread.sleep(time);
        }
        catch (Exception e_) {
            logger.log(e_.toString(), Level.SEVERE);
        }
    }

}
