/****************************************************************************
 * FILE: CustomerSeederTest.java
 * DSCRPT:
 ****************************************************************************/

package com.freeingreturns.catalogs.customer;

import static java.text.MessageFormat.*;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.junit.Test;

import com.freeingreturns.catalogs.customer.CustomerCatalogMain;
import com.freeingreturns.catalogs.customer.driver.DbSeeder;
import com.neeve.trace.Tracer;
import com.neeve.trace.Tracer.Level;

// @Ignore
public class CustomerSeederTest extends AbstractTest {
    private static final Tracer logger = Tracer.create("tests.basic", Level.DEBUG);

    private Properties setupEnv() {
        Properties env = new Properties();
        env.put("nv.ddl.profiles", "junit");

        env.put("x.apps.cst-processor.storage.clustering.enabled", "false");
        env.put("x.apps.cst-dbseeder.storage.clustering.enabled", "false");
        env.put("x.apps.cst-basicflow.storage.clustering.enabled", "false");
        env.put("appdataroot", "target/testbed/rdat");
        return env;
    }

    @Test
    public void testFlowAdd() throws Throwable {
        Properties env = setupEnv();

        String name = getClass().getSimpleName();
        logger.log(format("starting: {0}", name), Level.INFO);

        startApp(CustomerCatalogMain.class, "cst-processor", "cst-instA", env);
        DbSeeder seeder = startApp(DbSeeder.class, "cst-dbseeder", "cst-utils", env);

        CountDownLatch latch = seeder.getLatch();
        latch.await(10000, TimeUnit.MILLISECONDS);

        logger.log(format("ending: {0}", name), Level.INFO);
    }
}
